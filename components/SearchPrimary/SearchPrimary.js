import React from 'react'
import Icon from '../Icon'
import Input from '../Input'
import CircleIconPrimary from '../CircleIconPrimary'
const SearchPrimary = ({ className, placeholder, name, type, disabled, inputStyle }) => {
  return (
    <>
      <div className="p-8">
        <div className={className}>
{/*           <input className="input-search" id="search" type="text" placeholder="Search" /> */}
          <Input
            className="input-search w-0"
            inputStyle={{}}
            name="search"
            placeholder="Please enter something"
            type="text"
          />
          <div class="py-2 px-3">
            <CircleIconPrimary
              className="circle-icon-primary"
              colorIcon="icon-white"
              nameIcon="SearchIcon"
              optionsType="outline"
            />
          </div>
        </div>
      </div>
    </>
  )
}

export default SearchPrimary;