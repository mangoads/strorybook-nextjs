import React from 'react';
import SearchPrimary from './SearchPrimary';

export default {
  title: 'MangoAds/Atoms/Search',
  component: SearchPrimary,
}

const Template = (args) => <SearchPrimary {...args}/>

export const Primary = Template.bind({})
Primary.args = {
    className: 'search-primary',
}

