import React, { useState } from 'react'

const Bullets = ({ total = 1, className }) => {
  const [itemActive, setItemActive] = useState(-1)
  const render = () => {
    let result = []
    for (let i = 0; i <= total - 1; i++) {
      result.push(<li
        onClick={() => setItemActive(i)}
        className={`${itemActive === i ? "active" : ""} `} > </li>)
    }

    return result
  }
  return <ul className={className ? className : "bullet-primary"}>
    {render()}
  </ul>
}

export default Bullets;