import React from 'react';
import ButtonInfo from './ButtonInfo';

export default {
  title: 'MangoAds/Atoms/Button',
  component: ButtonInfo,
  argTypes: { onClick: { action: 'clicked' } },
}

const Template = (args) => <ButtonInfo {...args}/>

export const Info = Template.bind({})
Info.args = {
  className: 'button-info',
  message: 'Button Info',
  disabled: false,
  buttonStyle: {},
  iconName: 'MapIcon',
  reverse: false,
}

