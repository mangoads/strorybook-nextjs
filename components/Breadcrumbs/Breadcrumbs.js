import React from 'react'
import Link from 'next/link'
const Breadcrumbs = ({ active, message, onClick, href, className }) => {

  return (

    <li onClick={onClick} className={`${className ? className : "breadcrumb"} ${active ? "active" : ""}`}>
      {href ? <Link disabled={href ? true : false} href={href} >{message}</Link> : message}
    </li>

  )

}

export default Breadcrumbs;