import React from 'react'

const Heading4 = ({ message, className }) => {
    return <h4 className={className ? className : "heading4"}>
        {message}
    </h4>
}

export default Heading4;