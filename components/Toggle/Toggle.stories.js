import React from 'react';
import Toggle from './Toggle';

export default {
  title: 'MangoAds/Atoms/Form/Toggle',
  component: Toggle,
  argTypes: {
    onChange: { action: " change " }
  }
}

const Template = (args) => <Toggle {...args} />

export const primary = Template.bind({})
primary.args = {
  className: "toggle",
  message: "Toggle ME",
  disabled: false,
  checked: false
}

