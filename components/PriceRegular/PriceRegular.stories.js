import React from 'react';
import PriceRegular from './PriceRegular';

export default {
  title: 'MangoAds/Atoms/Price',
  component: PriceRegular,
  argTypes: {
    regular: {control: {type: 'number',min: 0}, table: {category: 'Price'}},
  }
}

const Template = (args) => <PriceRegular {...args}/>

export const Regular = Template.bind({})
Regular.args = {
    className: 'price-regular',
    regular: 10000,
    priceStyle: {}
}

