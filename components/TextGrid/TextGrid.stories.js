import React from 'react';
import TextGrid from './TextGrid';
import { action } from '@storybook/addon-actions';
const gridText = [
  'Unlimited projects',
  'No per user fees',
  'Unlimited storage',
  '24/7 support',
  'Cancel any time',
  '14 days free',
]
export default {
  title: 'MangoAds/Atoms/Text',
  component: TextGrid,
}

const Template = (args) => <TextGrid {...args} />

export const Grid = Template.bind({})
Grid.args = {
  nameIcon: 'CheckIcon',
  colorIcon: 'icon-success',
  className: 'text-list',
  cols: 3,
  data: gridText
}


