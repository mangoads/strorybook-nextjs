import React from 'react'
import { StarIcon as StarIconOl } from '@heroicons/react/outline'
import { StarIcon as StarIconSo } from '@heroicons/react/solid'
import ReactStars from "react-rating-stars-component";
import Icon from '../Icon'

const Rating = ({ max, size, className, activeColor }) => {
  const ratingChanged = (newRating) => {
    console.log(newRating);
  };
  return (
    <ReactStars
    className={className ? className : 'rating-primary'}
    count={5}
    onChange={ratingChanged}
    size={24}
    activeColor={activeColor}
  />
  )
}
export default Rating;