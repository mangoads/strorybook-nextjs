import React from 'react'

const Divider = ({ className, style }) => {
  return (
    <div className={className?.length != 0 ? className : 'divider-primary'} style={style}></div>
  )
}

export default Divider;