import React from 'react';
import CircleIconSuccess from './CircleIconSuccess';
import { action } from '@storybook/addon-actions';
export default {
  title: 'MangoAds/Atoms/CircleIcon',
  component: CircleIconSuccess,
  argTypes: {
    optionsType: {
      control: {
        type: 'inline-radio',
        options: ['solid', 'outline'],
      },
      table: { category: 'Options' },
      defaultValue: "outline"
    },
    onClick: { action: 'clicked' },
  }
}

const Template = (args) => <CircleIconSuccess {...args}/>

export const Success = Template.bind({})
Success.args = {
    className: 'circle-icon-success',
    nameIcon: 'HomeIcon',
}

