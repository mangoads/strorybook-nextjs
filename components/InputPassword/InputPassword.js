import React, { useState } from 'react'
import Icon from '../Icon'
import Input from '../Input'
const InputPassword = ({ className, inputStyle, placeholder }) => {

    const [passwordShown, setPasswordShown] = useState(false);
    const togglePasswordVisiblity = () => {
        setPasswordShown(passwordShown ? false : true);
    };
    return (
        <>
            <div className={className?.length != 0 ? className : 'link-primary'} style={inputStyle}>
                <Input
                    className="input-primary border-none"
                    name="password"
                    placeholder={placeholder}
                    type={passwordShown ? "text" : "password"}
                />
                {passwordShown ? (
                    <Icon className="icon-secondary"
                        name="EyeOffIcon"
                        onClick={togglePasswordVisiblity}
                    />
                ) : (
                    <Icon
                        className="icon-secondary"
                        name="EyeIcon"
                        onClick={togglePasswordVisiblity}
                    />
                )}
            </div>
        </>
    )
}

export default InputPassword;
