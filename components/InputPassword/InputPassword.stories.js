import React from 'react';
import InputPassword from './InputPassword';
export default {
  title: 'MangoAds/Atoms/Input',
  component: InputPassword,
  argTypes: {
    type: {control: {type: ''}}
  }
}

const Template = (args) => <InputPassword {...args}/>

export const Password = Template.bind({})
Password.args = {
    className: 'input-password',
    placeholder: 'Please enter your password',
    inputStyle: {}
}

