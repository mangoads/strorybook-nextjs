import React from 'react';
import Heading from './Heading';

export default {
  title: 'MangoAds/Atoms/Heading',
  component: Heading,
}

const Template = (args) => <Heading {...args} />

export const Heading1 = Template.bind({})
Heading1.args = {
  message: 'h1 display 1',
  className: "heading1"
}

