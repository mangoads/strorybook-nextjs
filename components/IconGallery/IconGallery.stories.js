import React from 'react';
import IconGallery from './IconGallery';
import { action } from '@storybook/addon-actions';
export default {
  title: 'MangoAds/Atoms/Icon',
  component: IconGallery,
  argTypes: {
    optionsType: {control: {type: ''}},
    className: {
      control: {
        type: 'select',
        options: ['icon-primary', 'icon-warning', 'icon-danger', 'icon-secondary', 'icon-success', 'icon-white', 'icon-black']
      },
      defaultValue: 'icon-primary'
    }
  }
}

const Template = (args) => <IconGallery {...args}/>

export const Gallery = Template.bind({})
Gallery.args = {}

