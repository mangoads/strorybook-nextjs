import React from 'react'
import * as Icons from "@heroicons/react/solid"
import Icon from "../Icon"
const IconGallery = ({className}) => {
    const iconList = React.useMemo(() => {
        const list = []
        for (const icon in Icons) {
            list.push(icon)
        }
        return list;
    })
    const getNameIcon = (nameIcon) => {
        alert("Icon Clicked: " + nameIcon)
    }
    return (
        <>
            {iconList.map((icon, index) => (
                <div key={index} className="inline-block">
                    <Icon
                        className={className}
                        name={icon}
                        optionsType="outline"
                        onClick={() => getNameIcon(icon)}
                    />
                    
                </div>
            ))}
        </>
    )
}

export default IconGallery;