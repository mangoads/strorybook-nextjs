import React from 'react'

const Select = ({ className, data }) => {
  return (
      <select
        id="location"
        name="location"
        className={className?.length != 0 ? className : 'select-primary'}
        defaultValue="Canada"
      >
        {
          data.map((value) => (
            <option key={value.id}>{value.option}</option>
          ))
        }
      </select>
  )
}

export default Select;