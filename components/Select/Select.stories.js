import React from 'react';
import Select from './Select';

const listOptions = [
  { id: 1, option: "Canada" },
  { id: 2, option: "USA" },
  { id: 3, option: "Vietnam" }
]
export default {
  title: 'MangoAds/Atoms/Form/Select',
  component: Select,
}

const Template = (args) => <Select {...args}/>

export const Primary = Template.bind({})
Primary.args = {
    className: 'select-primary',
    data: listOptions
}

