import React from 'react'

const TextArea = ({ className, rows, message, textAreaStyle, placeholder, disabled }) => {
  return (
    <textarea
      rows={rows}
      className={className?.length != 0 ? className : 'textarea-primary'} 
      disabled={disabled}
      placeholder={placeholder}
      style = {textAreaStyle}
    >{message}</textarea>
  )
}

export default TextArea;