import React from 'react'

const LoaderText = ({ className, loaderStyle, message }) => {
    return (
        <span 
        className={className?.length != 0 ? className : 'loader-ping'}
            style={loaderStyle}
        >
            {message}
        </span>
    )
}

export default LoaderText;