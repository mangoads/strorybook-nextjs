import React from 'react'
import img from "../../static/index.jpg"
const Image = ({ src = "", alt = "", width, height, className }) => {
  return <img loading="lazy" className={className ? className : "image"} src={src ? src : img} style={{ width: width, height: height }} />
}

export default Image;