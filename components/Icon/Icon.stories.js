import React from 'react';
import Icon from './Icon';
import { action } from '@storybook/addon-actions';
export default {
  title: 'MangoAds/Atoms/Icon',
  component: Icon,
  argTypes: {
    optionsType: {
      control: {
        type: 'inline-radio',
        options: ['solid', 'outline'],
      },
      defaultValue: "outline"
    },
    onClick: { action: 'clicked' },
    className: {
      control: {
        type: 'select',
        options: ['icon-primary', 'icon-warning', 'icon-danger', 'icon-secondary', 'icon-success', 'icon-white', 'icon-black']
      },
      defaultValue: 'icon-primary'
    }
  }
}

const Template = (args) => <Icon {...args}/>

export const Primary = Template.bind({})
Primary.args = {
    name: 'CakeIcon',
    className: 'icon-primary',
    iconStyle: {}
}

