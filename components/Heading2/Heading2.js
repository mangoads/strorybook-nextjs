import React from 'react'

const Heading2 = ({ message, className }) => {
    return <h2 className={className ? className : "heading2"}>
        {message}
    </h2>
}

export default Heading2;