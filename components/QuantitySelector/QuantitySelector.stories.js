import React from 'react';
import QuantitySelector from './QuantitySelector';

export default {
  title: 'MangoAds/Atoms/QuantitySelector',
  component: QuantitySelector,
  argTypes: {
    onIncrement: { action: "Increment" },
    onDecrement: { action: "Decrement" }
  }
}

const Template = (args) => <QuantitySelector {...args} />

export const Primary = Template.bind({})
Primary.args = {
  quantity: 1,
  disabledDecrease: false,
  disabledIncrease: false,
  disabledLabel: false,
  className: "quantity",

}

