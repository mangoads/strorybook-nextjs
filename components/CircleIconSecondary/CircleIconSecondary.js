import React from 'react'
import Icon from '../Icon'
const CircleIconSecondary = ({className, nameIcon, optionsType, onClick, colorIcon}) => {
  return (
    <div className={className?.length != 0 ? className : 'circle-icon-primary'} onClick={onClick} >
      <Icon
        className={colorIcon}
        name={nameIcon}
        optionsType={optionsType}
      />
    </div>
  )
}

export default CircleIconSecondary;