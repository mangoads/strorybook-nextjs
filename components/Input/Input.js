import React from 'react'
const Input = ({ className, placeholder, name, type, disabled, inputStyle}) => {
  return (
    <>
      <input
        type={type.length ? type : "password"}
        name={name.length != 0 ? name : 'email'}
        disabled = {disabled}
        id="email"
        className={className.length ? className : "input-primary"}
        placeholder={placeholder}
        style={inputStyle}
      />
    </>
  )
}

export default Input;