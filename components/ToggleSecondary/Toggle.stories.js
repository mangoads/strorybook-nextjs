import React from 'react';
import ToggleSecondary from './ToggleSecondary';

export default {
  title: 'MangoAds/Atoms/Form/Toggle',
  component: ToggleSecondary,
  argTypes: {
    onChange: { action: " change " }
  }
}

const Template = (args) => <ToggleSecondary {...args} />

export const secondary = Template.bind({})
secondary.args = {
  className: "toggle",
  message: "Toggle ME",
  checked: false,
  disabled: false
}

