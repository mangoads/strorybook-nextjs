import React, { useEffect, useState } from 'react'

const Radio = ({ checked, label, name, value, className, onChange, disabled }) => {
  const [check, setCheck] = useState(false)
  useEffect(() => {
    setCheck(checked)
  }, [checked])
  const handleOnChange = (e) => {
    onChange(e)
    setCheck(!check)
  }

  return <div className={className ? className : "radio"}>

    <input
      onChange={handleOnChange}
      disabled={disabled} type="radio" name={name} value={value} checked={check} />
    <label className="label" >{label}</label>
  </div>
}

export default Radio;