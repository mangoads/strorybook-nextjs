import React from 'react'

const Heading6 = ({ message, className }) => {
    return <h6 className={className ? className : "heading6"}>
        {message}
    </h6>
}

export default Heading6;