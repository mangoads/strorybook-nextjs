import React from 'react';
import Heading6 from './Heading6';

export default {
  title: 'MangoAds/Atoms/Heading',
  component: Heading6,
}

const Template = (args) => <Heading6 {...args} />

export const heading6 = Template.bind({})
heading6.args = {
  message: 'h6 display 6',
  className: "heading6"
}

