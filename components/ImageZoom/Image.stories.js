import React from 'react';
import Image from './Image';

export default {
  title: 'MangoAds/Atoms/Image',
  component: Image,
}

const Template = (args) => <Image {...args} />

export const Zoom = Template.bind({})
Zoom.args = {
  className: "image-zoom",
  src: "",
  alt: "",
  width: "200px",
  height: "200px"
}

