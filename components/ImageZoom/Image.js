import React from 'react'
import img from "../../static/index.jpg"
const Image = ({ src = "", alt = "", width, height, className }) => {
  return <img style={{ width: width, height: height }} loading="lazy" className={className ? className : "image-zoom"} src={src ? src : img} alt={alt} />
}

export default Image;