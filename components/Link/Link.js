import React from 'react'
import LinkNext from "next/link"
const Link = ({ type, className,link, title, message}) => {
  return(
    <LinkNext href={link}>
      <a 
        className={className?.length != 0 ? className : 'link-primary'}
        title={title}
      >
        {message}
      </a>
    </LinkNext>
  )
}

export default Link;