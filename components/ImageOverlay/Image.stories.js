import React from 'react';
import Image from './Image';

export default {
  title: 'MangoAds/Atoms/Image',
  component: Image,
}

const Template = (args) => <Image {...args} />

export const Overlay = Template.bind({})
Overlay.args = {
  className: "image",
  src: "",
  alt: "",
  width: "200px",
  height: "200px",

  overlayText: "overlay custom text"
}

