import React from 'react'
import img from "../../static/index.jpg"
const Image = ({ src = "", alt = "", width, height, overlayText, className }) => {
  return <div className={className ? className : "image"} style={{ width: width, height: height }}>
    <img loading="lazy" src={src ? src : img} alt={alt} />
    <div className="overlay">
      <p>{overlayText}</p>
    </div>
  </div>
}

export default Image;