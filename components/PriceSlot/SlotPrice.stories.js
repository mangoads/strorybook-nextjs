import React from 'react';
import SlotPrice from './SlotPrice';

export default {
  title: 'MangoAds/Atoms/Price',
  component: SlotPrice,
}

const Template = (args) => <SlotPrice {...args}/>

export const Slot = Template.bind({})
Slot.args = {
    className: 'price-slot',
    price: '100.00',
    priceStyle: {}
}

