import React from 'react'
const SlotPrice = ({ className, price }) => {
  return (
    <span
      className={className?.length != 0 ? className : 'price-regular'}
    >
      ${price}
    </span>
  )
}

export default SlotPrice;