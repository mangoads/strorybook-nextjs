import React from 'react';
import ButtonWhite from './ButtonWhite';

export default {
  title: 'MangoAds/Atoms/Button',
  component: ButtonWhite,
  argTypes: { onClick: { action: 'clicked' } },
}

const Template = (args) => <ButtonWhite {...args}/>

export const White = Template.bind({})
White.args = {
  className: 'button-white',
  message: 'Button White',
  disabled: false,
  buttonStyle: {},
  iconName: 'MapIcon',
  reverse: false
}

