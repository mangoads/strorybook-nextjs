import React from 'react'

const Overlay = ({ className, children }) => {
  return(
    <div className={className?.length != 0 ? className : 'overlay-primary'}>{children}</div>
  )
}

export default Overlay;