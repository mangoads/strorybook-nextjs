import React from 'react';
import ButtonDanger from './ButtonDanger';

export default {
  title: 'MangoAds/Atoms/Button',
  component: ButtonDanger,
  argTypes: { 
    onClick: { action: 'clicked', table: {category: 'Events'} },
    className: {
      control: {
        type: 'select',
        options: ['button-primary', 'button-secondary', 'button-success', 'button-warning', 'button-white', 'button-dark', 'button-danger', 'button-info']
      },
      defaultValue: 'button-danger'
    },
    reverse: {table: {category: 'Icon'}},
  },
}

const Template = (args) => <ButtonDanger {...args}/>

export const Danger = Template.bind({})
Danger.args = {
  message: 'Button Danger',
  disabled: false,
  buttonStyle: {},
  iconName: 'MapIcon',
  reverse: false
}

