import React from 'react'

const TextDefault = ({ children, className }) => {
  return (
    <p className= {className ? className : ''}>
      {children}
    </p>
  )
}

export default TextDefault;