import React from 'react';
import TextDefault from './TextDefault';
import { action } from '@storybook/addon-actions';

export default {
  title: 'MangoAds/Atoms/Text',
  component: TextDefault,
}

const Template = (args) => <TextDefault {...args} />

export const Default = Template.bind({})
Default.args = {
  className: 'text-default',
  children: 'this is paragraph'
}


