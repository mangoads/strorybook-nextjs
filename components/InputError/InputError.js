import React from 'react'
const Input = ({ className, placeholder, name, showError, messageError, inputStyle }) => {
  return (
    <>
      <input
        type="text"
        name={name.length != 0 ? name : 'email'}
        id="email"
        className={className.length ? className : "input-primary"}
        placeholder={placeholder}
        style={inputStyle}
      />
      {showError ? (
        <p className="mt-2 text-sm text-red-600" id="email-error">
          {messageError}
        </p>
      ) : ''}
    </>
  )
}

export default Input;