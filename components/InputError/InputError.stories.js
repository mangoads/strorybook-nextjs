import React from 'react';
import InputError from './InputError';

export default {
  title: 'MangoAds/Atoms/Input',
  component: InputError,
  argTypes: {
    type: {control: {type: ''}}
  }
}

const Template = (args) => <InputError {...args}/>

export const Error = Template.bind({})
Error.args = {
    className: 'input-error',
    placeholder: 'you@example.com',
    name: 'error',
    showError: true,
    messageError: 'Your password must be less than 4 characters.',
    inputStyle: {}
}

