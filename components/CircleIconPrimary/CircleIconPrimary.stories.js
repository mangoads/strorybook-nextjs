import React from 'react';
import CircleIconPrimary from './CircleIconPrimary';
import { action } from '@storybook/addon-actions';
export default {
  title: 'MangoAds/Atoms/CircleIcon',
  component: CircleIconPrimary,
  argTypes: {
    optionsType: {
      control: {
        type: 'inline-radio',
        options: ['solid', 'outline'],
      },
      table: { category: 'Options' },
      defaultValue: "outline"
    },
    onClick: { action: 'clicked' },
    className: {
      control: {
        type: 'select',
        options: ['circle-icon-primary', 'circle-icon-secondary', 'circle-icon-success', 'circle-icon-danger', 'circle-icon-warning', 'circle-icon-white', 'circle-icon-black'],
      },
      defaultValue: "circle-icon-primary"
    },
    colorIcon: {
      control: {
        type: 'select',
        options: ['icon-primary', 'icon-warning', 'icon-danger', 'icon-secondary', 'icon-success', 'icon-white', 'icon-black']
      },
      defaultValue: 'icon-white'
    },
  }
}

const Template = (args) => <CircleIconPrimary {...args} />

export const Primary = Template.bind({})
Primary.args = {
  nameIcon: 'HomeIcon',
}

