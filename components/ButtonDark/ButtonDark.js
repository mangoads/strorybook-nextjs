import React from 'react'
import { ExclamationIcon } from '@heroicons/react/solid'
import Icon from '../Icon'
function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}
const ButtonDark = ({ type, className, disabled, onClick, buttonStyle, message, reverse, iconName }) => {
  return (
    <button
      onClick={onClick}
      className={
        classNames(className?.length != 0 ? className : 'button-primary',
        disabled ? 'button-disabled' : '')
      } 
      type={type?.length != 0 ? type : 'button'}
      disabled={disabled}
      style={buttonStyle}
    >
      {message}
      {iconName ? 
        <Icon
          className={`icon-white ${reverse ? 'float-left' : 'float-right'}`}
          name={iconName}
          optionsType="outline"
        />
      : ''}
    </button>
  )
}

export default ButtonDark;