import React from 'react';
import ButtonDark from './ButtonDark';

export default {
  title: 'MangoAds/Atoms/Button',
  component: ButtonDark,
  argTypes: { 
    onClick: { action: 'clicked' },
    className: {
      control: {
        type: 'select',
        options: ['button-primary', 'button-secondary', 'button-success', 'button-warning', 'button-white', 'button-dark', 'button-danger', 'button-info']
      },
      defaultValue: 'button-dark'
    },
   },
}

const Template = (args) => <ButtonDark {...args}/>

export const Dark = Template.bind({})
Dark.args = {
  message: 'Button Dark',
  disabled: false,
  buttonStyle: {},
  iconName: 'MapIcon',
  reverse: false,
}

