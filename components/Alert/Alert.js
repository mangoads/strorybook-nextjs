import { useEffect, useState } from "react"

const Alert = ({ open = true, message = "message alert", className, title }) => {
    const [show, setShow] = useState("")
    useEffect(() => {
        setShow(open ? "" : "hide")
    }, [open])
    return (

        <div className={`${(className ? className : "alert alert-primary")} ${show}`} role="alert">
            {title && <svg xmlns="http://www.w3.org/2000/svg" className="icon" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
            </svg>}
            <div className="title-wrapper">
                <p className="title">
                    {title}
                </p>
                <span className="message" >{!title && <svg xmlns="http://www.w3.org/2000/svg" className="icon" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>}  {message}</span>
            </div>
            <span className="icon-wrapper">
                <svg onClick={() => {
                    setShow("hide")
                }} className="icon-close" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z" /></svg>
            </span>
        </div>

    )
}

export default Alert
