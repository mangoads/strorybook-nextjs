import React from 'react'

const TextTitle = ({ children, className }) => {
  return (
    <h2 className= {className ? className : ''}>
      {children}
    </h2>
  )
}

export default TextTitle;