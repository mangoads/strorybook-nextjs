import React from 'react';
import LinkIcon from './LinkIcon';

export default {
  title: 'MangoAds/Atoms/Link',
  component: LinkIcon,
  argTypes: {
    colorIcon: {
      control: {
        type: 'select',
        options: ['icon-primary', 'icon-warning', 'icon-danger', 'icon-secondary', 'icon-success', 'icon-white', 'icon-black']
      },
      defaultValue: 'icon-primary'
    },
    className: {
      control: {
        type: 'select',
        options: ['link-icon', 'link-icon-rounded'],
      },
      defaultValue: "link-icon"
    },
  }
}

const Template = (args) => <LinkIcon {...args}/>

export const Icon = Template.bind({})
Icon.args = {
    link: 'https://mangoads.vn/',
    title: "Go to home page of MangoAds Company",
    linkStyle: {},
    nameIcon:'MapIcon'
}

