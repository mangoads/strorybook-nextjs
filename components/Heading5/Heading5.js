import React from 'react'

const Heading5 = ({ message, className }) => {
    return <h5 className={className ? className : "heading5"}>
        {message}
    </h5>
}

export default Heading5;