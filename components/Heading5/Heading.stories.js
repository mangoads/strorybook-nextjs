import React from 'react';
import Heading5 from './Heading5';

export default {
  title: 'MangoAds/Atoms/Heading',
  component: Heading5,
}

const Template = (args) => <Heading5 {...args} />

export const heading5 = Template.bind({})
heading5.args = {
  message: 'h5 display 5',
  className: "heading5"
}

