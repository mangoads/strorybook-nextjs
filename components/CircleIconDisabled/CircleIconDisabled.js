import { HomeIcon } from '@heroicons/react/solid';
import React from 'react'
import Icon from '../Icon'
const CircleIconDisabled = ({className, nameIcon, optionsType, onClick, colorIcon}) => {
  return (
    <div className={className?.length != 0 ? className : 'circle-icon-primary'} onClick={onClick} >
      <Icon
        className={colorIcon}
        name={nameIcon}
        optionsType={optionsType}
   
      />
    </div>
  )
}

export default CircleIconDisabled;