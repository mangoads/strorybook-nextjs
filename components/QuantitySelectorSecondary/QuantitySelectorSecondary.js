import React, { useEffect, useState } from 'react'

const QuantitySelectorSecondary = ({ onIncrement, onDecrement, quantity, disabledIncrease, disabledDecrease, disabledLabel, className }) => {
  const [quantityState, setQuantityState] = useState(1)
  useEffect(() => {
    if (quantity) return setQuantityState(quantity)
    setQuantityState(0)
  }, [quantity])
  const handleClickDecrease = () => {
    onDecrement()
    setQuantityState(+quantityState - 1)
  }
  const handleClickIncrease = () => {
    onIncrement()
    setQuantityState(+quantityState + 1);
  }
  const handleOnChange = (e) => {
    if (!isNaN(e.target.value)) {
      setQuantityState(e.target.value)
    }
  }
  return <div className={className ? className : "quantity-secondary"}>
    <button disabled={disabledDecrease} onClick={handleClickDecrease}>-</button>

    <input disabled={disabledLabel} size="1" onChange={handleOnChange} type="text" value={quantityState} />

    <button disabled={disabledIncrease} onClick={handleClickIncrease}>+</button>
  </div>
}

export default QuantitySelectorSecondary;