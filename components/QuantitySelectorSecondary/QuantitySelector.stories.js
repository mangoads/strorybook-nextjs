import React from 'react';
import QuantitySelectorSecondary from './QuantitySelectorSecondary';

export default {
  title: 'MangoAds/Atoms/QuantitySelector',
  component: QuantitySelectorSecondary,
  argTypes: {
    onIncrement: { action: "Increment" },
    onDecrement: { action: "Decrement" }
  }
}

const Template = (args) => <QuantitySelectorSecondary {...args} />

export const Secondary = Template.bind({})
Secondary.args = {
  quantity: 1,
  disabledDecrease: false,
  disabledIncrease: false,
  disabledLabel: false,
  className: "quantity-secondary"
}

