
import Select from '../Select'
import Input from '../Input'
const InputTelephone = ({className, data, placeholder, inputStyle}) => {
  return (
    <div className={className?.length != 0 ? className : 'input-primary'} style={inputStyle}>
      <Select
        className="select-primary border-r-0"
        data={data}
      />
      <Input
        className="input-primary"
        name="email"
        placeholder={placeholder}
        type="tel"
      />
    </div>
  )
}
export default InputTelephone