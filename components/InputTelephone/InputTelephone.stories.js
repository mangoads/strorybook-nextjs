import React from 'react';
import InputTelephone from './InputTelephone';

const listOptions = [
  { id: 1, option: "US" },
  { id: 2, option: "CA" },
  { id: 3, option: "EU" }
]
export default {
  title: 'MangoAds/Atoms/Input',
  component: InputTelephone,
  argTypes: {
    type: {control: {type: ''}}
  }
}

const Template = (args) => <InputTelephone {...args}/>

export const Telephone = Template.bind({})
Telephone.args = {
    className: 'input-telephone',
    placeholder: 'Plase enter your numberphone',
    data: listOptions,
    inputStyle: {}

}

