import React from 'react'
import Icon from '../Icon'
import Input from '../Input'
import CircleIconPrimary from '../CircleIconPrimary'
import { SearchIcon } from '@heroicons/react/solid'
const SearchAnimation = ({ className, animation }) => {
  return (
    <div className="sm:h-20 relative z-0 flex-1 px-2 flex items-center justify-center sm:absolute sm:inset-0">
      <div className="w-full sm:max-w-xs">
        <label htmlFor="search" className="sr-only">
          Search
        </label>
        <div className="relative">
          <div className="pointer-events-none absolute inset-y-0 left-0 pl-3 flex items-center">
          <Icon
            className="h-5 w-5 text-gray-400"
            iconStyle={{}}
            name="SearchIcon"
            onClick={() => {}}
            optionsType="outline"
          />
          </div>
          <input
            id="search"
            name="search"
            className={`${animation ? 'w-0 transition-width ease-linear duration-500 focus:w-full': 'w-full'} search-animation`}
            placeholder="Search"
            type="search"
          />
        </div>
      </div>
    </div>
  )
}

export default SearchAnimation;