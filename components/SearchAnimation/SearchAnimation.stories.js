import React from 'react';
import SearchAnimation from './SearchAnimation';

export default {
  title: 'MangoAds/Atoms/Search',
  component: SearchAnimation,
}

const Template = (args) => <SearchAnimation {...args}/>

export const Animation = Template.bind({})
Animation.args = {
    className: 'search-primary',
    animation: true,
}

