import React from 'react'

const Heading3 = ({ message, className }) => {
    return <h3 className={className ? className : "heading3"}>
        {message}
    </h3>
}

export default Heading3;