import React from 'react';
import PriceSales from './PriceSales';

export default {
  title: 'MangoAds/Atoms/Price',
  component: PriceSales,
  argTypes: {
    regular: { control: { type: 'number', min: 0 }, table: { category: 'Price' } },
    special: { control: { type: 'number', min: 0 }, table: { category: 'Price' } },
  }
}

const Template = (args) => <PriceSales {...args} />

export const Sales = Template.bind({})
Sales.args = {
  className: 'price-sales',
  regular: 10000,
  priceOld: 50000,
  priceStyle: {}
}

