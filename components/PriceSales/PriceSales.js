import React from 'react'
import PriceRegular from '../PriceRegular'
import PriceOld from '../PriceOld'
const PriceSales = ({ className, regular, priceOld }) => {
  return (
    <div className={className?.length != 0 ? className : 'price-regular'}>
      <PriceRegular
        className="price-regular"
        regular={regular}
        priceStyle={{}}
      />
      <PriceOld
        className="price-special"
        priceOld={priceOld}
        priceStyle={{}}
      />
    </div>
  )
}
export default PriceSales;