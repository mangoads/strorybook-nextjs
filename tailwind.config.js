module.exports = {
  purge: [],
  darkMode: false,
  // or 'media' or 'class'
  theme: {
    extend: {
      transitionProperty: {
				'width': 'width',
				'spacing': 'margin, padding',
			},
    },
  },
  variants: {
    extend: {
      opacity: ['disabled'],
      width: ['focus'],
      display: ['hover']
    },
  },

}

